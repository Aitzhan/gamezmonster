using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private Button _startButton;
    [SerializeField] private Button _difficultyButton;
    [SerializeField] private Button _exitButton;
    [SerializeField] private MenuManager _manager;

    private void Start()
    {
        _startButton.onClick.AddListener(() => GameManager.Instance.ActivateGamePlayScene());
        _exitButton.onClick.AddListener(() => Application.Quit(0));

        _difficultyButton.onClick.AddListener(() => _manager.ActivateMenuState(false));
    }

    public void SetActive(bool status)
    {
        gameObject.SetActive(status);
    }
}