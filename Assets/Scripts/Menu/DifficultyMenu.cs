using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class DifficultyMenu : MonoBehaviour
{
    [SerializeField] private Button _easyButton; 
    [SerializeField] private Button _mediumButton;
    [SerializeField] private Button _hardButton;
    [SerializeField] private Button _backButton;
    [SerializeField] private MenuManager _menuManager;

    private void Start()
    {
        _easyButton.onClick.AddListener(() => SetDifficulty(Difficulty.Easy));
        _mediumButton.onClick.AddListener(() => SetDifficulty(Difficulty.Medium));
        _hardButton.onClick.AddListener(() => SetDifficulty(Difficulty.Hard));
        if (_menuManager != null)
            _backButton.onClick.AddListener(() => _menuManager.ActivateMenuState(true));
    }

    private void SetDifficulty(Difficulty difficulty)
    { 
        GameManager.Instance.DifficultyMode = difficulty;
    }
    public void SetActive(bool status)
    {
        gameObject.SetActive(status);
    }
        
}