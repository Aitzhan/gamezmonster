using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameMenu : MonoBehaviour
{
    public static GameMenu Instance;
    public bool IsDead { get; private set; } = false;

    [SerializeField] private TMP_Text _durationText;
    [SerializeField] private TMP_Text _attemptsText;
    [SerializeField] private GameObject _gameMenu;
    [SerializeField] private Button _restartButton;
    private float _duration;
    private int _attempts;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    private void Start()
    {
        _restartButton.onClick.AddListener(() => GameManager.Instance.ActivateGamePlayScene());
        SetAttempts();
    }

    private void SetAttempts()
    {
        _attempts = PlayerPrefs.GetInt("attempts");
        _attempts++;
        PlayerPrefs.SetInt("attempts", _attempts);
    }

    public void GameOver()
    {
        IsDead = true;
        _gameMenu.SetActive(true);
        _attemptsText.text = $"NumberOfAttempts: {PlayerPrefs.GetInt("attempts").ToString()}";
    }

    private void Update()
    {
        if (IsDead) return;
        _duration += Time.deltaTime;
        _durationText.text = $"Game Duration: {_duration.ToString("N0")} seconds";
    }
}