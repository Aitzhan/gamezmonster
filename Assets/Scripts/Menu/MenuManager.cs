using UnityEngine;

public class MenuManager : MonoBehaviour
{
    [SerializeField] private MainMenu _mainMenu;
    [SerializeField] private DifficultyMenu _difficultyMenu;
    
 
    public void ActivateMenuState(bool isMainMenu)
    {
        _difficultyMenu.SetActive(!isMainMenu);
        _mainMenu.SetActive(isMainMenu);
        
    }
}
