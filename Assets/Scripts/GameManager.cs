using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
   public static GameManager Instance;
   public Difficulty DifficultyMode { get; set; } = Difficulty.Easy;
   [SerializeField] private float delay;
   private const string gamePlaySceneName = "GamePlayScene";
   public float Delay
   {
      get => delay;
   }

   private void Awake()
   {
      if (Instance == null)
         Instance = this;
      
      DontDestroyOnLoad(gameObject);
   }

   public void ActivateGamePlayScene()
   {
      SceneManager.LoadScene(gamePlaySceneName);
   }
}
