using UnityEngine;

public class Wall : MonoBehaviour
{
    [SerializeField] private float leftBorder;
    private float _speed = 3f;
    private float _spawRate = 1f;
    private float _minHeight = -2f;
    private float _maxHeight = 2f;
    private void Start()
    {
        _speed *= (int)GameManager.Instance.DifficultyMode;
    }

    private void Update()
    {
        transform.Translate(Vector3.left * _speed * Time.deltaTime);
        if (transform.localPosition.x < leftBorder)
            Destroy(gameObject);
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.GetComponent<Bird>())
        {
            GameMenu.Instance.GameOver();
            Destroy(other.transform.parent.gameObject);
        }
    }

    public void SetupWall(Transform level)
    {
        transform.position += Vector3.up * UnityEngine.Random.Range(_minHeight, _maxHeight);
        transform.SetParent(level.transform);
    }
}