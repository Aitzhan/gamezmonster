using System.Collections;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;


public class CreatingGameObject : MonoBehaviour
{
    [SerializeField] private Transform _wallPosition;
    private GameObject Level { get; set; }

    private GameObject _wallPrefab;
    private GameObject _currentWall;


    private void Start()
    {
        StartCoroutine(CreateAsset("Level"));
        StartCoroutine(CreateAsset("Wall"));
    }

    private IEnumerator CreateAsset(string prefabName)
    {
        AsyncOperationHandle<GameObject> _operationHandle = Addressables.LoadAssetAsync<GameObject>(prefabName);
        yield return _operationHandle;

        switch (prefabName)
        {
            case "Wall":
                _wallPrefab = _operationHandle.Result;
                StartCoroutine(WallSettings());
                break;
            case "Level":
                Level = Instantiate(_operationHandle.Result);
                break;
            default:
                break;
        }
    }

    private IEnumerator WallSettings()
    {
        while (true)
        {
            yield return new WaitForSeconds(GameManager.Instance.Delay);
            if (GameMenu.Instance.IsDead)
            {
                yield break;
            }

            _currentWall = Instantiate(_wallPrefab, _wallPosition.position, Quaternion.identity);
            _currentWall.GetComponent<Wall>().SetupWall(Level.transform);
        }
    }
}