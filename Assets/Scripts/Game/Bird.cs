using System.Collections;
using UnityEngine;

public class Bird : MonoBehaviour
{
    [SerializeField] private float _gravity = -9.8f;
    [SerializeField] private float _strenght = 5f;
    private Vector3 _direction;

    private void Start()
    {
        StartCoroutine(UpdateSpeed());
    }

    private IEnumerator UpdateSpeed()
    {
        while (true)
        {
            yield return new WaitForSeconds(GameManager.Instance.Delay);
            if (GameMenu.Instance.IsDead)
                yield break;
            _gravity -= 1f;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
        {
            _direction = Vector3.up * _strenght;
        }

        _direction.y += _gravity * Time.deltaTime;
        transform.Translate(_direction * Time.deltaTime);
    }
}