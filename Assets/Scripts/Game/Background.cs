using UnityEngine;

public class Background : MonoBehaviour
{
    [SerializeField] private float _animationSpeed = 1f;
    [SerializeField] private MeshRenderer _meshRenderer;

    private void Awake()
    {
        _animationSpeed += (int)GameManager.Instance.DifficultyMode;
    }

    private void Update()
    {
        _meshRenderer.material.mainTextureOffset += new Vector2(_animationSpeed * Time.deltaTime, 0f);
    }
}
